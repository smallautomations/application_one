terraform {
  backend "azurerm" {
    resource_group_name  = "app-one-main"
    storage_account_name = "apponemain"
    container_name       = "tf-state"
    key                  = "app-one.tfstate"
  }
}