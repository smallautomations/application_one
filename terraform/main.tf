resource "azurerm_resource_group" "rr__rg" {
  count    = 1
  name     = "${var.vv__rg_name}-${count.index}"
  location = var.vv__location
}

module "mm__network" {
  count  = 1
  source = "./modules/mm__network"

  in__location        = azurerm_resource_group.rr__rg[0].location
  in__rg_name         = azurerm_resource_group.rr__rg[0].name
  in__vnet_name       = var.vv__vnet_name
  in__subnet_aks_name = var.vv__subnet_aks_name
}

module "mm__aks" {
  count  = 1
  source = "./modules/mm__aks"

  in__location  = azurerm_resource_group.rr__rg[0].location
  in__rg_name   = azurerm_resource_group.rr__rg[0].name
  in__vnet_name = module.mm__network[0].out__vnet.name
  in__subnet_id = module.mm__network[0].out__subnet_aks.id
  in__aks_name  = var.vv__aks_name
}