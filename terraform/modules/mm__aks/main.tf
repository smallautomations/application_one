resource "azurerm_kubernetes_cluster" "rr__aks" {
  count               = 1
  name                = "${var.in__aks_name}${count.index}"
  resource_group_name = var.in__rg_name
  location            = var.in__location
  dns_prefix          = "${var.in__aks_name}${count.index}"

  network_profile {
    network_plugin     = "kubenet"
    load_balancer_sku  = "basic"
    service_cidr       = "10.10.50.0/24"
    dns_service_ip     = "10.10.50.10"
    pod_cidr           = "10.188.18.0/24"
    docker_bridge_cidr = "172.17.0.0/16"
  }

  default_node_pool {
    name           = "default"
    node_count     = 1
    vm_size        = "Standard_B2s"
    vnet_subnet_id = var.in__subnet_id
    os_disk_size_gb = 64
  }

  identity {
    type = "SystemAssigned"
  }
}