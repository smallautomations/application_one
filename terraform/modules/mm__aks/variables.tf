variable "in__location" {
  type = string
}

variable "in__rg_name" {
  type = string
}

variable "in__vnet_name" {
  type = string
}

variable "in__subnet_id" {
  type = string
}

variable "in__aks_name" {
  type = string
}