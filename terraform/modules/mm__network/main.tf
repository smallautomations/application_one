resource "azurerm_virtual_network" "rr__vnet" {
  count               = 1
  name                = "${var.in__vnet_name}-${count.index}"
  location            = var.in__location
  resource_group_name = var.in__rg_name
  address_space       = ["10.10.0.0/16"]
}

resource "azurerm_subnet" "rr__subnet_aks" {
  count                = 1
  name                 = "${var.in__subnet_aks_name}-${count.index}"
  resource_group_name  = var.in__rg_name
  virtual_network_name = azurerm_virtual_network.rr__vnet[0].name
  address_prefixes     = ["10.10.0.0/24"]
}