output "out__vnet" {
  description = "vnet"
  value       = azurerm_virtual_network.rr__vnet[0]
}

output "out__subnet_aks" {
  description = "aks subnet"
  value       = azurerm_subnet.rr__subnet_aks[0]
}