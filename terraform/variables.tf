variable "vv__location" {
  type = string
}

variable "vv__rg_name" {
  type = string
}

variable "vv__vnet_name" {
  type = string
}

variable "vv__subnet_aks_name" {
  type = string
}

variable "vv__aks_name" {
  type = string
}